# Explore Courses API Demos

The ExploreCourses API provides public access to course data for Stanford University.

The API is available for two languages: Java and Kotlin.  By default, our Kotlin support
is demonstrated using kotlin-jvm, but kotlin-mpp is available upon request.

The samples provided are setup as projects which can be opened in an IDE.  The Java project
is available as an Eclipse project, and the Kotlin project is available as an Intellij project.
There is nothing inherently required about these IDEs (eg, the Java code would easily work in Intellij)
but these samples use these IDES because they are the most common IDEs for the respective langauges.

Using an IDE's autosuggest feature is the best way to explore the API.  You can find a relevant object
and use autosuggest to see which functions/properties are available on that object.  There are also
some javadoc refdocs available in the `javadocs` folder.

# Kotlin project using Intellij
The Kotlin project is available in the `explorecourses-api-demo-kotlin` directory using Intellij.


# Java project using Eclipse
The Java project is available in the `explorecourses-api-demo-java` directory using Eclipse.


# Reference Documentation
The reference docs are located in the `javadocs` directory, and can be opened with any web browser.

# Recommended Usage
Rather than using the API in a critical path, users are encouraged to use the API to periodically fetch all relevant
data and cache this data into their own local datastore.  This enables faster performance and avoids cascading
failures if/when an upstream system experiences downtime.

# Warranty
Notwithstanding anything to the contrary, this API is provided as-is and without any warranties express or implied.  


