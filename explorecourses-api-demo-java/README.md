To import this project:
 1.  Open the "Eclipse" IDE.
 2.  Choose "File > Import"
 3.  In the left pane, expand "General > Existing Projects into Workspace", and click "Next"
 4.  In the import dialog, select this directory (explorecourses-api-demo-java) as the root directory and click "Finish".
 5.  Open the "CourseIteratorExample.java" and click the green "Play" button to run the demo.

It may take a few seconds for the demo to run.  It should start prining all the course subject
codes and titles.

