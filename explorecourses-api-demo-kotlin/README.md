To import this project:
 1.  Open the "Intellij" IDE.
 2.  Choose "File > Open"
 3.  Select this directory ("explorecourses-api-demo-kotlin") and click "Ok"
 4.  Select "Trust Project" (if prompted) and "Open in New Window"
 5.  Open the "CourseIteratorExample.kt" and click the green "Play" button in the left gutter near the main function.

It may take a few seconds for the demo to run.  It should start prining all the course subject
codes and titles.

