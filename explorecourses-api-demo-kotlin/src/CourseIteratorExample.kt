import edu.stanford.services.explorecourses.ExploreCoursesConnection

/** Prints a list of all courses offered at Stanford in the current academic year  */
fun main(args: Array<String>) {
    val connection = ExploreCoursesConnection()
    for (s in connection.getSchools())
        for (d in s.departments)
            for (c in connection.getCoursesByQuery(d.code))
                println(c.subjectCodePrefix + c.subjectCodeSuffix + ": " + c.title)
}
